#!/usr/bin/env python

import numpy
import rospy
import tf
import random
from turtlesim.srv import Kill, Spawn
from geometry_msgs.msg import PointStamped, PoseArray, Pose
from turtlesim.msg import Pose as TPose

dt = 0.1

def update_state(data, number):

   seq[number]       += 1 
   position[number,:] = numpy.array([[data.x,data.y]])
   center = numpy.average(position, axis=0)

   cmsg = PointStamped()
   cmsg.point.x       = center[0]
   cmsg.point.y       = center[1]
   cmsg.header.seq    = seq[number]
   cmsg.header.stamp  = rospy.Time.now()
   cmsg.header.frame_id = "world"
   publish_center.publish(cmsg)

   distance = TPose()
   distance.x = data.x - center[0]
   distance.y = data.y - center[1]
   distance.theta = data.theta
   publish_distance[number].publish(distance)

   poses[number].position.x = data.x 
   poses[number].position.y = data.y
   poses[number].position.z = number
   orientation = tf.transformations.quaternion_from_euler(0, 0, data.theta)
   poses[number].orientation.x = orientation[0]
   poses[number].orientation.y = orientation[1]
   poses[number].orientation.z = orientation[2]
   poses[number].orientation.w = orientation[3]

   pa = PoseArray()
   pa.poses = poses
   pa.header.seq    = seq[number]
   pa.header.stamp  = rospy.Time.now()
   pa.header.frame_id = "world"
   publish_poses.publish(pa)

def run():

   global seq, poses, publish_poses, publish_center, publish_distance, position
   rospy.init_node('unicycles', anonymous=True)
   cast = tf.TransformBroadcaster()
   
   ## SETUP Turtlesim
   rospy.wait_for_service('kill')
   kill  = rospy.ServiceProxy('kill', Kill)
   spawn = rospy.ServiceProxy('spawn', Spawn)
   
   # get a parameter from our private namespace
   number = rospy.get_param('~number',2)
   seq    = []
   poses = []
   publish_distance = []
   publish_center   = rospy.Publisher("center", PointStamped, queue_size=1)
   publish_poses    = rospy.Publisher("poses", PoseArray, queue_size=1)
   position = numpy.zeros([number,2])
   try:
      resp1 = kill("turtle1")
      for i in range(number):
         px = random.uniform(1, 10)
         py = random.uniform(1, 10)
         th = random.uniform(0, 2*numpy.pi)
         resp2 = spawn(px,py,th,"unicycle_%d" % i)
         poses.append(Pose())
         seq.append(0)
         publish_distance.append(rospy.Publisher("unicycle_%d/distance" % i, TPose, queue_size=1))
   except rospy.ServiceException as exc:
      print("Service did not process request: " + str(exc))

   for i in range(number):
      rospy.Subscriber("unicycle_%d/pose" % i, TPose, update_state, (i))
   rate = rospy.Rate(1/dt) # 10hz
   while not rospy.is_shutdown():

      cast.sendTransform((0.0, 0.0, 0.0),
         (0.0, 0.0, 0.0, 1.0),
         rospy.Time.now(),
         "map",
         "world")

      rate.sleep()

if __name__ == '__main__':
   try:
      run()
   except rospy.ROSInterruptException:
      pass