#!/usr/bin/env python

import numpy
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

K  = 10*numpy.eye(2)

def update_state(data, i):
    
    d     = -numpy.sqrt(data.x**2+data.y**2)
    gphi  = numpy.arctan2(-data.y,-data.x)
    phi   = data.theta
    error = phi - gphi
    error = numpy.arctan2(numpy.sin(error),numpy.cos(error))
    
    x = numpy.array([[d],
                     [error]])
    u = -numpy.matmul(K,x)
    action = Twist()
    action.linear.x  = u.item(0)
    action.angular.z = u.item(1)

    actuators[i].publish(action)

def run():

    global actuators
    rospy.init_node('controller', anonymous=True)
    n = rospy.get_param('/unicycles/number',2)
    actuators = []
    for i in range(n):
        rospy.Subscriber("unicycle_%d/distance" % i, Pose, update_state, (i))
        actuators.append(rospy.Publisher("unicycle_%d/cmd_vel" % i, Twist, queue_size=1))

    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass