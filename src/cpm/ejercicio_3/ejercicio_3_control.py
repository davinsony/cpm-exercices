#!/usr/bin/env python

import numpy
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

K  = 10*numpy.eye(2)

def update_state(data):
    
    d     = -numpy.sqrt(data.x**2+data.y**2)
    gphi  = numpy.arctan2(-data.y,-data.x)
    phi   = data.theta
    error = phi - gphi
    error = numpy.arctan2(numpy.sin(error),numpy.cos(error))
    
    x = numpy.array([[d],
                     [error]])
    u = -numpy.matmul(K,x)
    action = Twist()
    action.linear.x  = u.item(0)
    action.angular.z = u.item(1)

    actuator.publish(action)

def run():

    global actuator
    rospy.init_node('controller', anonymous=True)
    n = rospy.get_param('~number',0)
    rospy.Subscriber("unicycle_%d/distance" % n, Pose, update_state)
    actuator = rospy.Publisher("unicycle_%d/cmd_vel" % n, Twist, queue_size=1)

    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass