#!/usr/bin/env python

import numpy
import rospy
from geometry_msgs.msg import Wrench
from turtlesim.msg import Pose

K    = 1 
y    = 0
goal = 5.5
dt = 0.05

msg = Wrench()
goalmsg = Pose()
goalmsg.x = goal

def set_goal(data):
   global goal, goalmsg
   goalmsg   = data 
   goal = float(data.x)

def measure(data):
   global y
   y = float(data.x)

def control():

   global msg, pub, pub_reference, goal, K, y, dt

   e = y-goal
   u = - K*e

   msg.force.x = u
   pub.publish(msg)
   pub_reference.publish(goalmsg)

def run():

   ## Creacion del nodo y el publicador
   global dt, pub, pub_reference
   pub           = rospy.Publisher('particula/force', Wrench, queue_size=1)
   pub_reference = rospy.Publisher('particula/reference', Pose, queue_size=1)
   rospy.Subscriber('particula/goal', Pose, set_goal)
   rospy.Subscriber('particula/pose', Pose, measure)
   rospy.init_node('control', anonymous=True)
   rate = rospy.Rate(1/dt) # 10hz
   while not rospy.is_shutdown():
      control()
      rate.sleep()

if __name__ == '__main__':
   try:
      run()
   except rospy.ROSInterruptException:
      pass