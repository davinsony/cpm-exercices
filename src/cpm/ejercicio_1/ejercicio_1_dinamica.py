#!/usr/bin/env python

import numpy
import rospy
import random
from turtlesim.srv import Kill, Spawn
from geometry_msgs.msg import Twist, Wrench
from turtlesim.msg import Pose

A = numpy.array([
  [0,0],
  [0,0]]) 
B = numpy.array([
  [1,0],
  [0,1]]) 
C = numpy.array([
  [1,0],
  [0,1]])
x = numpy.zeros([2,1])
u = numpy.zeros([2,1])
y = numpy.zeros([2,1])
dt = 0.05

msg = Twist()

def update_input(data):
   global u
   u = numpy.array([[data.force.x],[data.force.y]])

def update_state(data):
   global x
   posx = float(data.x)
   posy = float(data.y)
   if(posx >= 11 or posx <= 0):
      x[0,0] = - 0.7*x[0,0]
   if(posy >= 11 or posy <= 0):
      x[1,0] = - 0.7*x[1,0]

def dinamica():
   global msg, pub, x, A, B, u, y, C, dt

   x = x + dt*(numpy.matmul(A,x) + numpy.matmul(B,u))
   y = numpy.matmul(C,x)

   msg.linear.x, msg.linear.y = y.item(0),y.item(1)
   #rospy.loginfo(msg)
   pub.publish(msg)

def run():

   ## SETUP Turtlesim
   rospy.wait_for_service('kill')
   kill  = rospy.ServiceProxy('kill', Kill)
   spawn = rospy.ServiceProxy('spawn', Spawn)
   
   try:
      resp1 = kill("turtle1")
      px = random.uniform(1, 10)
      py = random.uniform(1, 10)
      resp2 = spawn(px,py,0,"particula")
   except rospy.ServiceException as exc:
      print("Service did not process request: " + str(exc))

   ## Creacion del nodo y el publicador
   global pub, dt
   pub = rospy.Publisher('particula/cmd_vel', Twist, queue_size=1)
   rospy.Subscriber('particula/force', Wrench, update_input)
   rospy.Subscriber('particula/pose', Pose, update_state)
   rospy.init_node('particula', anonymous=True)
   rate = rospy.Rate(1/dt) # 10hz
   while not rospy.is_shutdown():
      dinamica()
      rate.sleep()

if __name__ == '__main__':
   try:
      run()
   except rospy.ROSInterruptException:
      pass