#!/usr/bin/env python

import numpy
import rospy
from geometry_msgs.msg import Wrench
from turtlesim.msg import Pose
from datetime import datetime as time

# Diferencial de tiempo / tiempo discreto
dt = 0.05

# Medida
y = numpy.zeros([2,1])

# Estimacion de la velocidad
time_last = 0
y_last = 0

# Controlador
K = numpy.array([
   [1,2]])

msg  = Wrench()
goalmsg = Pose()
goalmsg.x = 5.5

# Objetivo
goal = numpy.array([
  [goalmsg.x],
  [0]])  

def set_goal(data):
   global goal, goalmsg
   goalmsg   = data 
   goal[0,0] = float(data.x)

def measure(data):
   global y, y_last, time_last, v
   
   y[0,0] = float(data.x)

   if time_last != 0:
      time_now = time.now()
      delta_t  = time_now-time_last
      delta_t  = delta_t.total_seconds()
      delta_y  = y[0,0] - y_last
      y[1,0] = delta_y/delta_t
      time_last = time_now
      y_last = y[0,0] 
   else:
      time_last = time.now()

def control():

   global msg, goalmsg, pub, pub_reference, dt

   e = numpy.array([[y - goal]])
   u = - numpy.matmul(K,e)

   msg.force.x = u.item(0)
   pub.publish(msg)
   pub_reference.publish(goalmsg)

def run():

   ## Creacion del nodo y el publicador
   global dt, pub, pub_reference
   pub           = rospy.Publisher('particula/force', Wrench, queue_size=1)
   pub_reference = rospy.Publisher('particula/reference', Pose, queue_size=1)
   rospy.Subscriber('particula/goal', Pose, set_goal)
   rospy.Subscriber('particula/pose', Pose, measure)
   rospy.init_node('control', anonymous=True)
   rate = rospy.Rate(1/dt) # 10hz
   while not rospy.is_shutdown():
      control()
      rate.sleep()

if __name__ == '__main__':
   try:
      run()
   except rospy.ROSInterruptException:
      pass