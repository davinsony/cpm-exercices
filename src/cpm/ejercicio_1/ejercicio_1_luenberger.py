#!/usr/bin/env python

import numpy
import rospy
from geometry_msgs.msg import Wrench
from turtlesim.msg import Pose
from datetime import datetime as time

# Diferencial de tiempo / tiempo discreto
dt = 0.05

# Matriz dinamica
A = numpy.array([
  [0,1],
  [0,0]])

# Matriz medida
C = numpy.array([[1,0]])

# Matriz de estimacion
L = numpy.array([
  [20],
  [100]]) 

# Medida
y = 0

# Estados estimados
xhat = numpy.zeros([2,1])

# Controlador
K = numpy.array([
   [1,2]])

msg  = Wrench()
goalmsg = Pose()
goalmsg.x = 5.5

# Objetivo
goal = numpy.array([
  [goalmsg.x],
  [0]])  

def set_goal(data):
   global goal, goalmsg
   goalmsg   = data 
   goal[0,0] = float(data.x)

def measure(data):
   global y
   y = float(data.x)

def control():

   global msg, goalmsg, pub, pub_reference, dt
   global xhat, A, L, C, y, goal

   ehat = y - numpy.matmul(C,xhat)
   xhat = xhat + dt*(numpy.matmul(A,xhat) + numpy.matmul(L,ehat))

   #print(xhat)

   e = xhat - goal
   u = - numpy.matmul(K,e)

   msg.force.x = u
   pub.publish(msg)
   pub_reference.publish(goalmsg)

def run():

   ## Creacion del nodo y el publicador
   global dt, pub, pub_reference
   pub           = rospy.Publisher('particula/force', Wrench, queue_size=1)
   pub_reference = rospy.Publisher('particula/reference', Pose, queue_size=1)
   rospy.Subscriber('particula/goal', Pose, set_goal)
   rospy.Subscriber('particula/pose', Pose, measure)
   rospy.init_node('control', anonymous=True)
   rate = rospy.Rate(1/dt) # 10hz
   while not rospy.is_shutdown():
      control()
      rate.sleep()

if __name__ == '__main__':
   try:
      run()
   except rospy.ROSInterruptException:
      pass