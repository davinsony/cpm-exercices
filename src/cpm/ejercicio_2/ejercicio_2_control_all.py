#!/usr/bin/env python

import numpy
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

K  = numpy.eye(2)

def update_state(data, i):
    
    x = numpy.array([[data.x],
                     [data.y]])
    u = -(i+1)*numpy.matmul(K,x)
    action = Twist()
    action.linear.x = u.item(0)
    action.linear.y = u.item(1)

    actuators[i].publish(action)

def run():

    global actuators
    rospy.init_node('controller', anonymous=True)
    n = rospy.get_param('/particles/number',2)
    actuators = []
    for i in range(n):
        rospy.Subscriber("particle_%d/distance" % i, Pose, update_state, (i))
        actuators.append(rospy.Publisher("particle_%d/cmd_vel" % i, Twist, queue_size=1))

    rate = rospy.Rate(1) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass