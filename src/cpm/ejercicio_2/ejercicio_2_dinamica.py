#!/usr/bin/env python

import numpy
import rospy
import tf
import random
from turtlesim.srv import Kill, Spawn
from geometry_msgs.msg import PointStamped, Point32
from turtlesim.msg import Pose
from sensor_msgs.msg import PointCloud

dt = 0.1

def update_state(data, number):

   seq[number]       += 1 
   position[number,:] = numpy.array([[data.x,data.y]])
   center = numpy.average(position, axis=0)

   cmsg = PointStamped()
   cmsg.point.x       = center[0]
   cmsg.point.y       = center[1]
   cmsg.header.seq    = seq[number]
   cmsg.header.stamp  = rospy.Time.now()
   cmsg.header.frame_id = "world"
   publish_center.publish(cmsg)

   distance = Pose()
   distance.x = data.x - center[0]
   distance.y = data.y - center[1]
   publish_distance[number].publish(distance)

   points[number].x = data.x 
   points[number].y = data.y
   points[number].z = number

   pc = PointCloud()
   pc.points = points
   pc.header.seq    = seq[number]
   pc.header.stamp  = rospy.Time.now()
   pc.header.frame_id = "world"
   publish_points.publish(pc)

def run():

   global seq, points, publish_points, publish_center, publish_distance, position
   rospy.init_node('particles', anonymous=True)
   cast = tf.TransformBroadcaster()
   
   ## SETUP Turtlesim
   rospy.wait_for_service('kill')
   kill  = rospy.ServiceProxy('kill', Kill)
   spawn = rospy.ServiceProxy('spawn', Spawn)
   
   # get a parameter from our private namespace
   number = rospy.get_param('~number',2)
   seq    = []
   points = []
   publish_distance = []
   publish_center   = rospy.Publisher("center", PointStamped, queue_size=1)
   publish_points   = rospy.Publisher("points", PointCloud, queue_size=1)
   position = numpy.zeros([number,2])
   try:
      resp1 = kill("turtle1")
      for i in range(number):
         px = random.uniform(1, 10)
         py = random.uniform(1, 10)
         resp2 = spawn(px,py,0,"particle_%d" % i)
         points.append(Point32())
         seq.append(0)
         publish_distance.append(rospy.Publisher("particle_%d/distance" % i, Pose, queue_size=1))
   except rospy.ServiceException as exc:
      print("Service did not process request: " + str(exc))

   for i in range(number):
      rospy.Subscriber("particle_%d/pose" % i, Pose, update_state, (i))
   rate = rospy.Rate(1/dt) # 10hz
   while not rospy.is_shutdown():

      cast.sendTransform((0.0, 0.0, 0.0),
         (0.0, 0.0, 0.0, 1.0),
         rospy.Time.now(),
         "map",
         "world")

      rate.sleep()

if __name__ == '__main__':
   try:
      run()
   except rospy.ROSInterruptException:
      pass