#!/usr/bin/env python

import numpy
import rospy
import tf
from aruco_msgs.msg import MarkerArray

def publish_tf(data):

    for marker in data.markers:

        px = marker.pose.pose.position.x 
        py = marker.pose.pose.position.y 
        pz = marker.pose.pose.position.z 
        rx = marker.pose.pose.orientation.x 
        ry = marker.pose.pose.orientation.y 
        rz = marker.pose.pose.orientation.z 
        w  = marker.pose.pose.orientation.w 

        parent = data.header.frame_id
        child  = "marker_%d" % marker.id

        time = rospy.Time.now()

        cast.sendTransform( (px,py,pz),
                            (rx,ry,rz,w),
                            time,
                            child,
                            parent)

def run():

    global cast
    rospy.init_node('aruco_tf', anonymous=True)
    cast = tf.TransformBroadcaster()
    rospy.Subscriber("/aruco/markers", MarkerArray, publish_tf)
    rate = rospy.Rate(0.1) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass